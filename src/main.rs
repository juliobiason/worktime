extern crate chrono;

use std::env;
use chrono::*;

fn break_time(time: &String) -> (u32, u32) {
    let hour = time[0..2].to_string().parse::<i8>().unwrap();
    let minute = time[3..5].to_string().parse::<i8>().unwrap();

    (hour as u32, minute as u32)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Need time.");
        return;
    }

    let ref enter_time = args[1];
    let (hour, minute) = break_time(enter_time);

    let day_start = Local::now()
        .with_hour(hour as u32)
        .unwrap()
        .with_minute(minute as u32)
        .unwrap();

    println!("Enter datetime: {}", day_start.format("%H:%M"));
    let day_end_continuous = day_start + 
        Duration::hours(8) + 
        Duration::minutes(30);

    println!("Exit datetime: {}", day_end_continuous.format("%H:%M"));

    let timespan = day_end_continuous - day_start;
}
